# 我的记账系统 MyTally
## 功能
- 登录
- 添加账目
- 显示账目清单
- 统计分析显示折线图
- 导出表格Ecxel
- 关于

## 技术细节	
使用Java客户端swing编程，
使用MySQL存储数据，
使用BeautyEye美化原生的JFrame，
使用apache dbutils做数据库连接操作，
使用jfreechart做数据统计显示报表，
使用poi导出账单列表到Excel

## 数据库
- mysql
- mytally
- 根目录 mytally.sql

## 使用的框架
- [beautyeye](https://github.com/JackJiang2011/beautyeye)
- [jfreechart](https://github.com/jfree/jfreechart)

## 截图
### 登录
![登录](screenshots/login.png)

### 主页面
![主页面](screenshots/main.png)

### 添加账单
![添加账单](screenshots/addTally.png)

### 账单列表
![账单列表](screenshots/tallyList.png)

### 消费分析
![消费分析](screenshots/analysis.png)